import { ApolloClient } from 'apollo-boost';
import { gql } from 'apollo-boost'


export async function updateData(config, client, owner, type, size) {
  var recentData = await client
            .query({
              query: gql`
                        {
                          RecentData(size: ${size}, owner: "${owner}", type: "${type}"){
                            type
                            value
                            timestamp
                          }
                        }
                      `
    });
  // console.log(recentData);

  recentData = recentData.data.RecentData;

  config.data.datasets[0].label = type;

  if (recentData.length > 1 && recentData[0].timestamp > recentData[1].timestamp)
      recentData.reverse();
  config.data.labels = [];
  config.data.datasets[0].data = [];

  recentData.forEach(function (data) {
    config.data.labels.push(data.timestamp);
    config.data.datasets[0].data.push(data.value);
  });

  window.myLine.update();
}