// Using IIFE for Implementing Module Pattern to keep the Local Space for the JS Variable
import ApolloClient from 'apollo-boost';
import gql from 'graphql-tag';
import {loadProfile, loadSensorDetail, updateSensorDetail} from "./detail";
import {updateData} from "./data";

(async function() {

  const graphqlEndpoint = "http://3.17.149.13:3005/graphql"
  const client = new ApolloClient({
    uri: graphqlEndpoint
  });

  // graph display configuration
  var config = {
    type: 'line',
    data: {
      labels: [],
      datasets: [{
        label: " ",
        lineTension: 0,
        backgroundColor: window.chartColors.red,
        borderColor: window.chartColors.red,
        data: [],
        fill: false,
        borderWidth: 1.2,
        pointRadius: 0.7,
      }]
    },
    options: {
      responsive: true,
      title: {
        display: true,
        text: 'IoT sensor chart'
      },
      tooltips: {
        mode: 'index',
        intersect: false,
      },
      hover: {
        mode: 'nearest',
        intersect: true
      },
      scales: {
        xAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Time'
          },
          ticks: {
            max: 90,
            min: 10,
            stepSize: 5
          }
        }],
        yAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Value'
          },
          ticks: {
            max: 90,
            min: 10,
            stepSize: 5
          }
        }]
      },
      animation: {
        duration: 0.1

      }
    }
  };

  // initialize graph
  var ctx = document.getElementById('canvas').getContext('2d');
  window.myLine = new Chart(ctx, config);

  var profileInfo = await loadProfile("quyvo", client);
  var sensorDetail = await loadSensorDetail(config, "quyvo", client);
  await updateData(config, client, profileInfo.owner, sensorDetail.type, 12);

  setInterval(async () => {
    const client = new ApolloClient({
      uri: graphqlEndpoint
    });
    await updateSensorDetail(config, "quyvo", client);
    await updateData(config, client, profileInfo.owner, config.data.datasets[0].label, 12);
  }, 5000);


  })();
