
export async function ajax(url, method, payload, successCallback){
  var xhr = new XMLHttpRequest();
  await xhr.open(method, url, true);
  await xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  await xhr.onreadystatechange = function () {
    if (xhr.readyState != 4 || xhr.status != 200) return;
    successCallback(xhr.responseText);
  };
  await xhr.send(JSON.stringify(payload));
}