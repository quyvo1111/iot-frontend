import { ApolloClient } from 'apollo-boost';
import { gql } from 'apollo-boost';
import {updateData} from './data';

export async function loadSensorDetail(config, owner, client) {
  var sensorDetail = await client
    .query({
      query: gql`
                        {
                          getSensorDetailByOwner(owner: "${owner}"){
                            owner
                            type
                            address
                            description
                            start_time
                          }
                        }
                      `
    });
  sensorDetail = sensorDetail.data.getSensorDetailByOwner;

  for (var i = 0; i < sensorDetail.length; i++) {
    document.getElementById('sensorDetailFrame').appendChild(createSensorDetailBlock(i));
    document.getElementById('sensorDetailName' + i).innerHTML = sensorDetail[i].type;
    document.getElementById('sensorDetailAddress' + i).innerHTML = sensorDetail[i].address;
    document.getElementById('sensorDetailStartTime' + i).innerHTML = sensorDetail[i].start_time;
    const type = sensorDetail[i].type;

    var recentData = await client
      .query({
        query: gql`
                        {
                          RecentData(size: 1, owner: "${owner}", type: "${sensorDetail[i].type}"){
                            type
                            value
                            timestamp
                          }
                        }
                      `
      });
    console.log(recentData);

    recentData = recentData.data.RecentData;

    document.getElementById('sensorValue' + i).innerHTML = recentData[0].value;
    document.getElementById('sensorTimeStamp' + i).innerHTML = recentData[0].timestamp;
    document.getElementById('sensorChoose' + i).addEventListener('click',function () {
       updateData(config, client, owner, type, 10);
      }
    );
  }

  return sensorDetail[sensorDetail.length - 1]
}

export async function updateSensorDetail(config, owner, client) {
  var sensorDetail = await client
    .query({
      query: gql`
                        {
                          getSensorDetailByOwner(owner: "${owner}"){
                            owner
                            type
                            address
                            description
                            start_time
                          }
                        }
                      `
    });
  sensorDetail = sensorDetail.data.getSensorDetailByOwner;

  console.log('sensor detail -----: ',sensorDetail);
  for (var i = 0; i < sensorDetail.length; i++) {
    document.getElementById('sensorDetailName' + i).innerHTML = sensorDetail[i].type;
    document.getElementById('sensorDetailAddress' + i).innerHTML = sensorDetail[i].address;
    document.getElementById('sensorDetailStartTime' + i).innerHTML = sensorDetail[i].start_time;
    const type = sensorDetail[i].type;

    var recentData = await client
      .query({
        query: gql`
                        {
                          RecentData(size: 1, owner: "${owner}", type: "${sensorDetail[i].type}"){
                            type
                            value
                            timestamp
                          }
                        }
                      `
      });
    console.log(recentData);

    recentData = recentData.data.RecentData;

    document.getElementById('sensorValue' + i).innerHTML = recentData[0].value;
    document.getElementById('sensorTimeStamp' + i).innerHTML = recentData[0].timestamp;
    document.getElementById('sensorChoose' + i).addEventListener('click',function () {
        updateData(config, client, owner, type, 10);
      }
    );
  }

  return sensorDetail[sensorDetail.length - 1]
}

export async function loadProfile(profileId, client) {
  var profileInfo = await client
    .query({
      query: gql`
                        {
                          profile(id: "${profileId}"){
                            owner
                            name
                            email
                            company
                            telephone
                            address
                          }
                        }
                      `
    });

  document.getElementById('profile_owner').innerHTML = profileInfo.data.profile.owner;
  document.getElementById('profile_name').innerHTML = profileInfo.data.profile.name;
  document.getElementById('profile_email').innerHTML = profileInfo.data.profile.email;
  document.getElementById('profile_company').innerHTML = profileInfo.data.profile.company;
  document.getElementById('profile_address').innerHTML = profileInfo.data.profile.address;
  document.getElementById('profile_telephone').innerHTML = profileInfo.data.profile.telephone;

  return profileInfo.data.profile;

}

function createSensorDetailBlock(id) {
  var block = document.createElement('div');
  block.className = "pure-u-1 pure-u-md-1-3";
  block.innerHTML = ' ' +
    '            <div class="pricing-table pricing-table-biz pricing-table-selected">\n' +
    '                <div class="pricing-table-header">\n' +
    '                    <h2 id = "sensorDetailName' + id + '"></h2>\n' +
    '                    <h5 id="sensorDetailAddress' + id + '"></h5>\n' +
    '                    <h5 id="sensorDetailStartTime' + id + '"></h5>\n' +
    '\n' +
    '                    <span class="pricing-table-price" id="sensorValue' + id + '">\n' +
    '                    </span>\n' +
    '                    <h2 id="sensorTimeStamp' + id + '"></h2>\n' +
    '                </div>\n' +
    '                <button class="button-choose pure-button" id="sensorChoose' + id + '">Choose</button>\n' +
    '            </div>\n' +
    '        ';
  return block;

}