var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

var initTempData = {
  temperature : [19, 20, 21, 22, 30, 18],
  time : [1,2,3,4,5,6]
}

app.get('/getTemperature', function(req,res){
  res.send(initTempData);
});

app.get('/addTemperature', function(req,res){
  var temp = getRandomInt(10,40);
  var time = initTempData.time.length;
  initTempData.time.push(time);
  initTempData.temperature.push(temp)
  res.send({time: time, temperature: temp});
});

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Error Handler for 404 Pages
app.use(function(req, res, next) {
  var error404 = new Error('Route Not Found');
  error404.status = 404;
  next(error404);
});

module.exports = app;

app.listen(9000, function(){
  console.log('Example app listening on port 9000!')
});